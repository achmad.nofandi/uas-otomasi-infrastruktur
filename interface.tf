variable "gcp_project" {
  type        = string
  description = "uas-filkom"
}

variable "gcp_region" {
  type        = string
  description = "Target GCP region ID"
  default     = "us-central1"
}

variable "gcp_zone" {
  type        = string
  description = "Target GCP zone ID"
  default     = "us-central1-a"
}

variable "gcp_credential_file_path" {
  type        = string
  description = "Path of credential file (Service account key)"
  default     = "key.json"
}

variable "instance_name" {
  type        = string
  description = "Instance name"
  default     = "vm01"
}

variable "instance_machine_type" {
  type        = string
  description = "Instance machine type"
  default     = "f1-micro"
}

variable "instance_image" {
  type        = string
  description = "Instance image"
  default     = "ubuntu-os-cloud/ubuntu-1804-lts"
}

variable "instance_volume_size" {
  type        = string
  description = "Instance volume size"
  default     = "10"
}

variable "network_name" {
  type        = string
  description = "Network used for instance connectivity"
  default     = "default"
}

variable "instance_user" {
  type        = string
  description = "Instance access username"
  default     = "ubuntu"
}

variable "instance_pubkey_file_path" {
  type        = string
  description = "Path of access pubkey file"
  default     = "~/.ssh/id_rsa.pub"
}

variable "instance_cloudinit_file_path" {
  type        = string
  description = "Instance cloud-init script"
  default     = "cloud-init.yaml"
}

variable "dns_zone_name" {
  type        = string
  description = "Name of managed zone"
  default     = "example-com"
}

variable "dns_zone_domain" {
  type        = string
  description = "Domain name of managed zone"
  default     = "example.com"
}

variable "dns_record_name" {
  type        = string
  description = "DNS record name"
  default     = "blog.example.com"
}

output "INSTANCE_PUBLIC_IP" {
  value = google_compute_instance.blog.network_interface.0.access_config.0.nat_ip
}