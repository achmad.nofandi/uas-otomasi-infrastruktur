terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.37.0"
    }
  }
}

provider "google" {
  credentials = file(var.gcp_credential_file_path)
  project     = var.gcp_project
  region      = var.gcp_region
  zone        = var.gcp_zone
}

resource "google_dns_managed_zone" "dns_zone" {
  dns_name   = "${var.dns_zone_domain}."
  labels     = {}
  name       = var.dns_zone_name
  visibility = "public"
  timeouts {}
}

resource "google_dns_record_set" "dns_record" {
  managed_zone = google_dns_managed_zone.dns_zone.name
  name         = "${var.dns_record_name}."
  rrdatas      = [google_compute_instance.blog.network_interface.0.access_config.0.nat_ip]
  ttl          = 300
  type         = "A"
}

resource "google_compute_firewall" "http_access" {
  name = "allow-http-acces"
  network = var.network_name
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  target_tags = ["blog"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "https_access" {
  name = "allow-https-acces"
  network = var.network_name
  allow {
    protocol = "tcp"
    ports    = ["443"]
  }
  target_tags = ["blog"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_instance" "blog" {
  name         = var.instance_name
  machine_type = var.instance_machine_type
  tags = ["blog"]
  boot_disk {
    initialize_params {
      image = var.instance_image
      size  = var.instance_volume_size
    }
  }
  network_interface {
    network = var.network_name
    access_config {
    }
  }
  metadata = {
    ssh-keys  = "${var.instance_user}:${file(var.instance_pubkey_file_path)}"
    user-data = file(var.instance_cloudinit_file_path)
  }
}